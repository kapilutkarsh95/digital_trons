
import { initSlots } from "../../screens/HomeScreen/store/actions/index";
import AsyncStorage from '@react-native-community/async-storage';
const slots = [
    {  time : "9:00" , booked : false , bookingInfo: null },
{ time : "10:00" , booked : false , bookingInfo : null   },
{ time : "11:00" , booked : false , bookingInfo : null   },
{ time : "12:00" , booked : false , bookingInfo : null   },
{ time : "1:00" , booked : false , bookingInfo : null   },
{ time : "2:00" , booked : false , bookingInfo : null   },
{ time : "3:00" , booked : false , bookingInfo : null   },
{ time : "4:00" , booked : false , bookingInfo : null   }
]
export const initSlot = () => {
    return dispatch => {
      AsyncStorage.getItem("slots").then( value => {
         if ( !value ) {
             AsyncStorage.setItem("slots",JSON.stringify(slots)).then( result => {
                 console.log("result",result)
             } ).catch( err => {
                 console.log("error",err)
             } )
             dispatch(initSlots(slots))
         } else {
           dispatch( initSlots(JSON.parse(value)) );           
         }
      } ).catch( err => {
          console.log("error",err)
      } )
    }
}