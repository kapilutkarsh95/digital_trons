import React from "react";
import { Text , StyleSheet , TouchableOpacity , Platform } from 'react-native';
const slotButton = (props) => {
   return (
       <TouchableOpacity  style={ [styles.container , props.style ]  } onPress={ props.clicked }>
          <Text style={ styles.text } > { props.text } </Text>
       </TouchableOpacity>
   )
}
const styles = StyleSheet.create({
  container : {
      display : "flex",
      width : "30%" ,
      height : 70 ,
      justifyContent : "center" ,
      alignItems : "center" ,
  
      marginLeft : "2%",
      marginBottom : "4%",
      borderWidth : 1,
      borderRadius : 50 
  } ,
  text : {
    fontFamily : Platform.OS == "android" ? "Roboto" : "Helvetica" ,
    fontSize : 30 ,
    fontWeight : "500",
    color : "#fff"
  }
});
export default slotButton;