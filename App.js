import React , { Fragment , PureComponent } from "react";
import { Text , StatusBar , Platform } from "react-native";
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from "./screens/HomeScreen/HomeScreen";
import SlotInfoScreen from "./screens/SlotInfoScreen/SlotInfoScreen";
import { connect } from "react-redux";
import { initSlot } from "./store/actions/mainActions";
const Stack = createStackNavigator();
const forFade = ({current}) => ({
  cardStyle: {
    opacity: current.progress,
  },
});
const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,

    background: '#fff',
  },
};
class App extends PureComponent {
    componentDidMount() {
        console.log("App component mounted");
        this.props.initSlots();
    }
    render(){
  return (
      <Fragment>
          { Platform.OS == "android" && <StatusBar backgroundColor="#fff" /> }
          <NavigationContainer theme={MyTheme}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName="Home">
        
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen
            options={{cardStyleInterpolator: forFade}}
            name="SlotInfo"
            component={SlotInfoScreen}
          />
        </Stack.Navigator>
       
        </NavigationContainer>      
      </Fragment>
  )
        }
        }
const mapDispatchToProps = (dispatch) => {
    return {
    initSlots : () => dispatch( initSlot() )
    }
}
export default connect(null,mapDispatchToProps)(App);