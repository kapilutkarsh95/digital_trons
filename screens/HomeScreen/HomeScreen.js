import React , { PureComponent , Fragment } from "react";
import { FlatList , ScrollView , Text , SafeAreaView, StyleSheet , Platform , TouchableOpacity , PermissionsAndroid ,Image } from "react-native";
import SlotButton from "../../components/SlotButton/SlotButton";
import { setSlotTime , setFirstName , setLastName , setPhoneNumber } from "../SlotInfoScreen/store/actions/index";
import { connect } from "react-redux";

import CameraRoll from "@react-native-community/cameraroll";
class HomeScreen extends PureComponent {
    state = {
      data  :"" ,
      showImagePicker : false ,
      has_next_page : false ,
      end_cursor : null 
    
    }
    closeImagePicker = () => {
      console.log("close Image Picker");
      this.setState({
       showImagePicker : false ,
       has_next_page : false ,
       data : "",
       end_cursor : null 
      });
    }
    openImagePicker = async () => {
      if ( Platform.OS == "android" ) {
        const result = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          {
            title: 'Permission Explanation',
            message: 'ReactNativeForYou would like to access your photos!',
          },
        );
        if (result !== 'granted') {
          console.log('Access to pictures was denied');
          return;
        }
      }
      
      CameraRoll.getPhotos({
        first: 50,
        assetType: 'Photos',
      })
      .then(res => {
        console.log("page info",res.page_info , res.edges)
        this.setState({ has_next_page : res.page_info.has_next_page , end_cursor : res.page_info.end_cursor  });
        this.setState({ data: res.edges });
        this.setState({ showImagePicker : true  });
      })
      .catch((error) => {
         console.log(error);
      });
  }
   endReached = () => {
     console.log("end reached");
     if ( this.state.has_next_page ) {
      CameraRoll.getPhotos({
        first : 50,
        after : this.state.end_cursor,
        assetType: 'Photos',
      })
      .then(res => {
        console.log("page info",res.page_info)
        this.setState({ has_next_page : res.page_info.has_next_page , end_cursor : res.page_info.end_cursor  });
        const newData = [...this.state.data , ...res.edges];
        this.setState({ data : newData });
        // this.setState({ data: res.edges });
        // this.setState({ showImagePicker : true  });
      })
      .catch((error) => {
         console.log(error);
      });
     }
   }
    slotClicked = (slot) => {
        console.log("slot Clicked");
        if (slot.booked ) {
         this.props.setSlotTime(slot.time);
         this.props.setFirstName( slot.bookingInfo.firstname );
         this.props.setLastName( slot.bookingInfo.lastname );
         this.props.setPhoneNumber( slot.bookingInfo.phoneNumber );
        } else {
         this.props.setSlotTime( slot.time );
        }
        this.props.navigation.navigate("SlotInfo");
    }
 
    render(){
    
     return (
        <SafeAreaView style={ styles.container } >
            <ScrollView style={ styles.subContainer } >
            
               
                <Text style={ styles.text } > List of Slots  </Text>
              <FlatList
               numColumns={3}
               data={ this.props.slots }
               keyExtractor={item => item.time}
               renderItem={ ( {item} ) => <SlotButton style={ {backgroundColor : item.booked ? "red" : "green" } } text={ item.time } clicked={ () => this.slotClicked(item)} /> }
               />
              
                <TouchableOpacity
                 
                  style={ styles.imageButton }
                  onPress={ () =>  {
                    if ( !this.state.showImagePicker ) {
                      this.openImagePicker()
                    } else {
                     this.closeImagePicker();
                    }
                    // this.openImagePicker
                  } }>
                    { !this.state.showImagePicker ?    <Text style={styles.imageButtonText}> Pick Image </Text> :   <Text style={styles.imageButtonText}> Close Gallery </Text>}
                
                   
                </TouchableOpacity>
                { this.state.showImagePicker && (<FlatList
                     style={{ height : 300 , marginTop : 20   }}
                     data={this.state.data}
                     numColumns={3}
                     onEndReached={ this.endReached }
                     renderItem={({ item }) => (
                     <Image
                        style={{
                          width: '33%',
                          height: 150,
                        }}
                        source={{ uri: item.node.image.uri }} />
                        )}
                         /> )}
            </ScrollView>
        </SafeAreaView>
     )
 }
}
const styles = StyleSheet.create({
    imageButton : {
      display: 'flex',
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#009fee',
    borderRadius: 50,
    width: '60%',
    marginTop : "4%",
    marginLeft : "20%"
    },
    imageButtonText : {
      color: '#fff',
      fontSize: 20,
      fontFamily: 'Roboto',
    },
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
      },
      
      subContainer: {
        padding: '6%',
        display: 'flex',
        height: '100%',
        width: '100%',
      },
      text : {
        fontFamily : Platform.OS == "android" ? "Roboto" : "Helvetica" ,
        fontSize : 30 ,
        fontWeight : "700",
        textAlign : "center",
        fontStyle : "italic",
        marginBottom : "10%" 
      }
});
const mapStateToProps = (state) => {
  return {
   slots : state.homeScreen.slots 
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
  setSlotTime : (data) => dispatch( setSlotTime(data) ) ,
  setFirstName : (data) => dispatch( setFirstName(data) ) ,
  setLastName : (data) => dispatch( setLastName(data) ),
  setPhoneNumber : (data) => dispatch( setPhoneNumber(data) )
  }
}
export default connect( mapStateToProps , mapDispatchToProps )(HomeScreen);