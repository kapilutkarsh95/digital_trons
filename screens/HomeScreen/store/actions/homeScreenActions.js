import * as actions from "./actionTypes";
export const initSlots = (value) => {
  return {
      type : actions.INIT_SLOTS ,
      value 
  }
}
export const updateSlot = (data) => {
   return {
     type : actions.UPDATE_SLOT ,
     data 
   }
}