import * as actions from "../actions/actionTypes";
const initialState = {
  slots : []
}
const homeScreenReducer = ( state = initialState , action ) => {
  switch( action.type ) {
      case actions.UPDATE_SLOT:
        return {
          ...state ,
          slots : state.slots.map( slot => {
              if ( slot.time != action.data.slotTime ) {
                 return slot 
              }
               let updatedSlot = {
                 ...slot 
               }
             updatedSlot.booked = true
             updatedSlot.bookingInfo = {
               firstname : action.data.firstname ,
               lastname : action.data.lastname ,
               phoneNumber : action.data.phoneNumber
             } 
             return updatedSlot
          } )
        }
      case actions.INIT_SLOTS :
          return {
              ...state ,
              slots : action.value 
          }
    default :
    return state;
  }
}
export default homeScreenReducer;