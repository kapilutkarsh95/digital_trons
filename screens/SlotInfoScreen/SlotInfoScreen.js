import React , { PureComponent } from "react";
import { Text , StyleSheet , SafeAreaView , ScrollView , Platform , TextInput ,View , TouchableOpacity  } from "react-native";
import { connect } from "react-redux";
import { setFirstName , setLastName , setPhoneNumber , resetInfo } from "./store/actions/index";
import { updateSlot , initSlots } from "../HomeScreen/store/actions/index";
import AsyncStorage from '@react-native-community/async-storage';
class SlotInfoScreen extends PureComponent {
  cancel = () => {
    this.props.resetInfo();
    this.props.navigation.navigate("Home");
  }
  save = async () => {
    console.log("save");
    const data = {
      slotTime : this.props.slotTime ,
      firstname : this.props.firstname ,
      lastname : this.props.lastname ,
      phoneNumber : this.props.phoneNumber
    }
    const updatedSlots = this.props.slots.map( slot => {
      if ( slot.time != data.slotTime ) {
         return slot 
      }
       let updatedSlot = {
         ...slot 
       }
     updatedSlot.booked = true
     updatedSlot.bookingInfo = {
       firstname : data.firstname ,
       lastname : data.lastname ,
       phoneNumber : data.phoneNumber
     } 
     return updatedSlot
  } )
     
     await AsyncStorage.setItem("slots",JSON.stringify(updatedSlots));
     this.props.initSlots(updatedSlots);
    // this.props.updateSlot(data);
    this.props.resetInfo();
    this.props.navigation.navigate("Home");
  }
  render() {
    let saveButtonDisabled = false 
    let saveButtonStyles = [styles.nextButton];
    if ( this.props.firstname == "" || this.props.lastname == "" || this.props.phoneNumber == "" ) {
      saveButtonDisabled = true;
      saveButtonStyles = [ styles.nextButton , styles.disableCss ]
    } 
    return (
     <SafeAreaView style={ styles.container } >
       <ScrollView style={ styles.subContainer } >
           <Text style={ styles.text } > Enter Details to book slot  </Text>
           <TextInput
                  value={this.props.firstname}
                  onChangeText={text => this.props.setFirstName(text) }
                  placeholderTextColor="rgb(227,227,227)"
                  style={styles.input}
                  placeholder="First Name"
                />
              <TextInput
                  value={this.props.lastname}
                  onChangeText={text => this.props.setLastName(text) }
                  placeholderTextColor="rgb(227,227,227)"
                  style={styles.input}
                  placeholder="Last Name"
                />
                 <TextInput
                  value={this.props.phoneNumber}
                  onChangeText={text => this.props.setPhoneNumber(text) }
                  placeholderTextColor="rgb(227,227,227)"
                  style={styles.input}
                  placeholder="Phone Number"
                />
                   <View style={styles.footerSection} >
                   <TouchableOpacity
                  onPress={() => this.cancel()}
                  style={styles.backButton}
                   >
                  <Text style={styles.backButtonText}> Cancel </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={ saveButtonDisabled }
                  style={ saveButtonStyles }
                  onPress={() => this.save()}>
                  <Text style={styles.nextButtonText}> Save </Text>
                </TouchableOpacity>
                   </View>
       </ScrollView>
     </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  disableCss: {
    opacity: 0.4,
  },
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
  
  subContainer: {
    padding: '6%',
    display: 'flex',
    height: '100%',
    width: '100%',
  },
  text : {
    fontFamily : Platform.OS == "android" ? "Roboto" : "Helvetica" ,
    fontSize : 30 ,
    fontWeight : "700",
    textAlign : "center",
    fontStyle : "italic",
    marginBottom : "10%" 
  },
  input: {
    paddingBottom: 20,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontSize: 20,
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgb(227,227,227)',
  },
  footerSection: {
    display: 'flex',
    
    height: '15%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  backButton: {
    backgroundColor: 'rgb(224,234,252)',
    width: '30%',
    height: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  backButtonText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: 'rgb(37,111,238)',
    fontWeight: 'bold',
  },
  nextButton: {
    backgroundColor: 'rgb(37,111,238)',
    width: '30%',
    height: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: '#fff',
    fontWeight: 'bold',
  },
})
const mapStateToProps = (state) => {
 return {
   slots : state.homeScreen.slots ,
   slotTime : state.slotInfo.slotTime ,
  firstname : state.slotInfo.firstname ,
  lastname : state.slotInfo.lastname ,
  phoneNumber : state.slotInfo.phoneNumber
 }
}
const mapDispatchToProps = (dispatch) => {
  return {
    initSlots : (data) => dispatch( initSlots(data) ),
    updateSlot : (data) => dispatch( updateSlot(data) ),
  setFirstName : (data) => dispatch( setFirstName(data) ) ,
  setLastName : (data) => dispatch( setLastName(data) ),
  setPhoneNumber : (data) => dispatch( setPhoneNumber(data) ) ,
  resetInfo : () => dispatch( resetInfo() )
  }
}
export default connect( mapStateToProps , mapDispatchToProps )(SlotInfoScreen);