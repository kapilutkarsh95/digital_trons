export {
  setFirstName ,
  setLastName ,
  setPhoneNumber ,
  resetInfo ,
  setSlotTime
} from "./slotInfoScreenActions";