import * as actions from "./actionTypes";
export const setFirstName = (value) => {
    return {
        type : actions.SET_FIRST_NAME ,
        value 
    }
}
export const setLastName = (value) => {
    return {
        type : actions.SET_LAST_NAME ,
        value 
    }
}
export const setPhoneNumber = (value) => {
    return {
        type : actions.SET_PHONE_NUMBER ,
        value 
    }
}
export const resetInfo = () => {
    return {
        type : actions.RESET_INFO
    }
}
export const setSlotTime = (value) => {
    return {
        type : actions.SET_SLOT_TIME ,
        value 
    }
}