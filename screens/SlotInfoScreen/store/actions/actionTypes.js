export const SET_FIRST_NAME = "SET_FIRST_NAME";
export const SET_LAST_NAME = "SET_LAST_NAME";
export const SET_PHONE_NUMBER = "SET_PHONE_NUMBER";
export const RESET_INFO = "RESET_INFO";
export const SET_SLOT_TIME = "SET_SLOT_TIME";