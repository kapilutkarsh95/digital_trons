import * as actions from "../actions/actionTypes";
const initialState = {
  slotTime : "",  
  firstname : "",
  lastname : "",
  phoneNumber : ""
}
const slotInfoReducer = ( state=initialState , action ) => {
  switch ( action.type ) {
      case actions.SET_SLOT_TIME:
          return {
              ...state ,
              slotTime : action.value 
          }
      case actions.RESET_INFO:
          return {
              ...state ,
              firstname : "",
              lastname :"",
              phoneNumber : "",
              slotTime : ""
          }
      case actions.SET_FIRST_NAME :
          return {
              ...state ,
              firstname : action.value 
          }
     case actions.SET_LAST_NAME:
         return {
             ...state ,
             lastname : action.value
         }     
     case actions.SET_PHONE_NUMBER:
         return {
             ...state ,
             phoneNumber : action.value
         }    
     default :
     return state; 
  }
}
export default slotInfoReducer;