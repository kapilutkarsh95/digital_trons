/**
 * @format
 */
import React from 'react';
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import homeScreenReducer from "./screens/HomeScreen/store/reducer/homeScreenReducer";
import slotInfoReducer from "./screens/SlotInfoScreen/store/reducer/slotInfoScreenReducer";
import {Provider} from 'react-redux';
import {name as appName} from './app.json';
import thunk from 'redux-thunk';
const logger = store => {
    return next => {
      return action => {
        console.log('Action Dispatched', action);
        const result = next(action);
        console.log('Updated State', store.getState());
        return result;
      };
    };
  };
const rootReducer = combineReducers({
  homeScreen : homeScreenReducer ,
  slotInfo : slotInfoReducer
});
const store = createStore( rootReducer , applyMiddleware( thunk,  logger) );
const RNRedux = () => (
    <Provider store={store}>
      <App />
    </Provider>
  );
AppRegistry.registerComponent(appName, () => RNRedux );
